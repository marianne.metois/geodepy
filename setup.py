
from setuptools import setup, find_packages

setup(name='geodepy',
      version='0.3.4',
      description=('tools for PCA on GPS time series '),
      author='Marianne Metois',
      author_email='marianne.metois@univ-lyon1.fr',
      url='https://bitbucket.org/mmetois/geodepy',
      license='CeCILL',
      classifiers=[
          # How mature is this project? Common values are
          # 3 - Alpha
          # 4 - Beta
          # 5 - Production/Stable
          'Development Status :: 4 - Beta',
          'Intended Audience :: Science/Research',
          'Intended Audience :: Education',
          'Topic :: Scientific/Engineering',
          'License :: OSI Approved',  # CeCILL-B License, similar to BSD,
          # Specify the Python versions you support here. In particular,
          # ensure that you indicate whether you support Python 2,
          # Python 3 or both.
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.4'
      ],
      packages=find_packages(exclude=['doc', 'tests']),
      install_requires=['numpy', 'matplotlib', 'scipy',
                        'pandas', 'scikit-learn'],
      entry_points={'console_scripts':
                    ['gpspca = geodepy.gpspca:main']})
