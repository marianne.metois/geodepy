#!/usr/bin/env python
"""
Make EMPCA from time series

"""


from __future__ import division, print_function

import argparse
import os
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA
from scipy.optimize import curve_fit

from geodepy.empca import empca


def func(x, a, b):
    return a*x + b


def main():

    parser = argparse.ArgumentParser(
        description=__doc__)
    parser.add_argument('-file', help='file with stations names')
    parser.add_argument('-loc', help='path to the .neu files')
    parser.add_argument(
        '-tspan', help='start and end dates in yyyy-mm-dd', nargs=2)
    parser.add_argument(
        '-cp', help='components to use format "neu", "ne", "eu" etc')
    parser.add_argument(
        '-sm', help='smoothing to apply to the eigen vectors',
        type=int, default=0)
    parser.add_argument(
        '-it', help='number of iterations, default is 25',
        type=int, default=25)
    parser.add_argument(
        '-eg', help='number of eigen vectors', type=int, default=5)
    parser.add_argument(
        '-sp', help='sampling rate of time series, defaults is D', default='D')
    parser.add_argument(
        '-save',
        help='set 1 to write eigen vectors and reconstructed ts, default is 0',
        type=int, default=0)
    parser.add_argument(
        '-sigm',
        help=('set 1 to use 1/sigma**2 for weight, '
              'default is 0 meaning 1/sigma. '
              'Set to "no" for not using uncertainties'), default='0')
    parser.add_argument(
        '-ica', help='do you want ica check ? yes is 1', default=0)
    parser.add_argument('-out', help='output directory', default='tmp')
    parser.add_argument(
        '-dtr', help='a priori detrend ? Default is no, i.e 0',
        type=int, default=0)

    args = parser.parse_args()

    if args.cp == 'neu':
        comp = (1, 2, 3)
    elif args.cp == 'ne':
        comp = (1, 2)
    elif args.cp == 'nu':
        comp = (1, 3)
    elif args.cp == 'eu':
        comp = (2, 3)
    elif args.cp == 'n':
        comp = (1,)
    elif args.cp == 'e':
        comp = (2,)
    elif args.cp == 'u':
        comp = (3,)
    else:
        print(args.cp, 'is not a possible combination use n,e,u')
        sys.exit("exit now!")

    compW = np.array(comp) + 3

    ##############
    # methode un peu ad-hoc pour eviter de perdre l'information quand on a des
    # heures de mesures pas vraiment toutes les 30s et qu'on fait de reindexage
    # mais super lourd. A reprendre a l'occasion if args.sp == '30S': dates =
    # pd.date_range(args.tspan[0], args.tspan[1], freq='S') else:
    dates = pd.date_range(args.tspan[0], args.tspan[1], freq=args.sp)

    # names = np.loadtxt(args.file, dtype=('|S20'))
    names = np.loadtxt(args.file, dtype=('U'))
    nbnames = len(names)

    if args.dtr:
        print('# WARNING automatic a-priori detrend with '
              'very uncontinuous time series can be a bad idea')

    for i, nn in enumerate(names):
        path = args.loc + nn + '.neu'

        data = np.loadtxt(
            path, delimiter='    ', comments='#', ndmin=2, usecols=comp)
        weight = np.loadtxt(
            path, delimiter='    ', comments='#', ndmin=2, usecols=compW)
        tps = np.loadtxt(
            path, delimiter='    ', comments='#', usecols=(7,), dtype=('U'))
        tok = np.loadtxt(
            path, delimiter='    ', comments='#', usecols=(0,))

        dt = pd.DataFrame(data, index=tps)
        dt['index'] = dt.index
        dt = dt.drop_duplicates(subset='index')
        del dt['index']
        dt.index = pd.DatetimeIndex(dt.index)

        wdt = pd.DataFrame(weight, index=tps)
        wdt['index'] = wdt.index
        wdt = wdt.drop_duplicates(subset='index')
        del wdt['index']
        wdt.index = pd.DatetimeIndex(wdt.index)

        tdy = pd.DataFrame(tok, index=tps)
        tdy['index'] = tdy.index
        tdy = tdy.drop_duplicates(subset='index')
        del tdy['index']
        tdy.index = pd.DatetimeIndex(tdy.index)

        if args.dtr:
            data2 = np.array(dt[args.tspan[0]:args.tspan[1]])
            weight2 = np.array(wdt[args.tspan[0]:args.tspan[1]])
            t = pd.DataFrame(tok, index=tps)
            t['index'] = t.index
            t = t.drop_duplicates(subset='index')
            del t['index']
            t.index = pd.DatetimeIndex(t.index)
            tok2 = np.array(t[args.tspan[0]:args.tspan[1]])

            for compo in range(data2.T.shape[0]):
                p_opt, p_cov = curve_fit(func, tok2.T[0], data2.T[compo],
                                         sigma=weight2.T[compo], p0=None)
                data2.T[compo] = data2.T[compo]-func(tok2.T, *p_opt)
            dt = pd.DataFrame(
                data2, index=t[args.tspan[0]:args.tspan[1]].index)
            wdt = pd.DataFrame(
                weight2, index=t[args.tspan[0]:args.tspan[1]].index)

        data_int = dt.reindex(index=dates).interpolate(
            method='linear').interpolate(method='barycentric').fillna(
                method='ffill').fillna(method='bfill')
        weight_int = wdt.reindex(index=dates).fillna(1000000)
        tdy_int = tdy.reindex(index=dates).interpolate(method='linear')

        if i == 0:
            M = np.array(data_int)
            W = np.array(weight_int)
        elif i == nbnames-1:
            M = np.vstack((M.T, np.array(data_int).T)).T
            if args.sigm == '1':
                W = 1./(np.vstack((W.T, np.array(weight_int).T)).T)**2
            else:
                W = 1./np.vstack((W.T, np.array(weight_int).T)).T
        else:
            M = np.vstack((M.T, np.array(data_int).T)).T
            W = np.vstack((W.T, np.array(weight_int).T)).T

    if (W > 10000).any():
        print('# WARNING there are gaps in the time-series you use '
              'that could lead to errors in the EMPCA -- '
              'check the interpolation method if needed')

    # center M
    M = M - np.mean(M, axis=0)

    ###################
    # EMPCA calling
    ###################
    if args.sigm == 'no':
        myPCA = empca(
            M.T,
            smooth=args.sm,
            nvec=args.eg,
            niter=args.it)
    else:
        myPCA = empca(
            M.T, W.T,
            smooth=args.sm,
            nvec=args.eg,
            niter=args.it)

    # print "First mode explains", myPCA.R2vec(0)*100, "% of the data variance"
    print("First mode explains {:.2f} % of the data variance".format(
        myPCA.R2vec(0)*100))

    ###################
    # PLOT COMMON MODES
    ###################

    fig, axes = plt.subplots(args.eg, 1, figsize=(10, 13))
    fig.suptitle('Eigen vectors', fontsize=20)

    if args.eg > 1:
        for ia, ax in enumerate(axes):
            ax.plot(dates, myPCA.eigvec[ia, :])
    else:
        axes.plot(dates, myPCA.eigvec[0, :])

    ####################
    # SAVING EG & COEFFS
    ####################

    if args.save:

        if not os.path.exists(args.out):
            os.makedirs(args.out)
        else:
            print(args.out,
                  'Output directory already exists ! '
                  'Remove it or use -out option')
            sys.exit("exit now!")

        fig.savefig(args.out + '/egs.png', bbox_inches='tight')

        ######################
        # PRINT MODES VARIANCE
        ######################
        v = []
        for i in range(len(myPCA.eigvec)):
            v.append(myPCA.R2vec(i)*100)
        np.savetxt(args.out + '/variances.txt', v)
        fig, axe = plt.subplots()
        fig.suptitle('Variances', fontsize=20)
        if args.eg > 1:
            plt.xticks(np.arange(min(range(len(myPCA.eigvec))),
                                 max(range(len(myPCA.eigvec)))+1, 1.0))
            axe.plot(range(len(myPCA.eigvec)), v[:], 'ro',
                     range(len(myPCA.eigvec)), v[:], 'k--')
            plt.ylabel('variance in %')
            plt.xlabel('mode number')
            fig.savefig(args.out + '/vars.png', bbox_inches='tight')
        #####################

        np.savetxt(args.out + '/coeffpca.txt', myPCA.coeff)
        np.savetxt(args.out + '/dates.txt', dates, fmt="%s")
        np.savetxt(args.out + '/dyr.txt', tdy_int)
        models = []

        t = pd.DataFrame(myPCA.eigvec.T, index=dates)
        t.to_csv(args.out + "/egs" + ".txt", sep=' ')

        for i in range(len(myPCA.eigvec)):
            models.append(np.outer(myPCA.coeff[:, i], myPCA.eigvec[i]).T)

        k = data.shape[1]

        for i, nn in enumerate(names):
            if k == 1:
                l = i
                np.savetxt(args.out + '/' + nn + ".obs." + args.cp,
                           np.vstack((tdy_int.T, myPCA.data[l, :],
                                      1./(W.T[l, :]))).T)

                ri = np.vstack(
                    (tdy_int.T, np.asarray(models[0:args.eg])[:, :, l]))
                mm = pd.DataFrame(ri.T, index=dates)
                mm.to_csv(args.out + '/' + nn + ".mod." + args.cp, sep=' ',
                          header=False, float_format='%.12f')

            elif k == 2:
                l = i+i
                np.savetxt(args.out + '/' + nn + ".obs." + args.cp,
                           np.vstack((tdy_int.T, myPCA.data[l, :],
                                      myPCA.data[l+1, :], 1./(W.T[l, :]),
                                      1./(W.T[l+1, :]))).T)

                ri = np.vstack(
                    (tdy_int.T, np.asarray(models[0:args.eg])[:, :, l],
                     np.asarray(models[0:args.eg])[:, :, l+1]))
                mm = pd.DataFrame(ri.T, index=dates)
                mm.to_csv(args.out + '/' + nn + ".mod." + args.cp, sep=' ',
                          header=False, float_format='%.12f')
            elif k == 3:
                l = i+i*2
                np.savetxt(args.out + '/' + nn + ".obs." + args.cp,
                           np.vstack((tdy_int.T, myPCA.data[l, :],
                                      myPCA.data[l+1, :],
                                      myPCA.data[l+2, :], 1./(W.T[l, :]),
                                      1./(W.T[l+1, :]), 1./(W.T[l+2, :]))).T)

                ri = np.vstack(
                    (tdy_int.T, np.asarray(models[0:args.eg])[:, :, l],
                     np.asarray(models[0:args.eg])[:, :, l+1],
                     np.asarray(models[0:args.eg])[:, :, l+2]))
                mm = pd.DataFrame(ri.T, index=dates)
                mm.to_csv(args.out + '/' + nn + ".mod." + args.cp, sep=' ',
                          header=False, float_format='%.12f')
            i = i+1

    plt.show()

    if args.ica:

        ica = FastICA(n_components=5, max_iter=15000, whiten='boolean')
        # Reconstruct signals
        S_ = ica.fit_transform(models[0]+models[1]+models[2] +
                               models[3]+models[4])
        # Get estimated mixing matrix
        # A_ = ica.mixing_

        fig, [ax1, ax2, ax3, ax4, ax5] = plt.subplots(5, 1, figsize=(10, 13))
        fig.suptitle('ICA', fontsize=20)
        ax1.plot(dates, S_[:, 0], color='red')
        ax2.plot(dates, S_[:, 1], color='red')
        ax3.plot(dates, S_[:, 2], color='red')
        ax4.plot(dates, S_[:, 3], color='red')
        ax5.plot(dates, S_[:, 4], color='red')

        plt.savefig(os.path.join(args.out+'/ica.png'))

        plt.show()


if __name__ == '__main__':
    main()
