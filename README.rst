GEODEPY tools
=============

This Python package provides the command ``gpspca``. Once installed, you can get
the help with::

  gpspca -h

This work uses the Weighted EMPCA code by Stephen Bailey,
available at https://github.com/sbailey/empca/

Installation
------------

First install the requirements (numpy, pandas, matplotlib, scipy and
scikit-learn). If you use conda, you can do::

  conda install --file requirements.txt

Then::

  make develop

which is just an alias of::

  python setup.py develop

If you do not have the permission to write in the python lib directory (meaning
you do not use virtualenv or conda), you can run instead::

  python setup.py develop --user

In this case, you may need to add ``~/.local/bin`` in the PATH environment
variable.

Test
----

You can check if a basic command works with::

  make try

This is just an alias of the test command::

  gpspca -file tests/list.txt -loc tests/ -tspan 2006-06-01 2012-10-24 -cp e -save 1  -it 25 -sm 30
