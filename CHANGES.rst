
0.3.4 (31-10-2017)
------------------

- duplicate records detection by R. Grandin
- prior to PCA detrend on the specific time span, by M. Metois & R. Grandin
- no uncertainty option -sigm no, by M. Metois
- print the explained data variance for each mode & screen for mode 1, by
  M. Metois
- adapted for PCA conducted on high rate GPS time series (-sp option)
